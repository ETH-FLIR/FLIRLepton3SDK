/*
 * FLIR Lepton3 SDK
 */

#ifndef YOCTO_I2C_COMMUNICATOR_H
#define YOCTO_I2C_COMMUNICATOR_H

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <linux/i2c.h>

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include "I2CCommunicator.h"

class YoctoI2CCommunicator : public I2CCommunicator
{
public:
  YoctoI2CCommunicator(const char* filename);
  ~YoctoI2CCommunicator();
  int beginTransaction(uint8_t slaveAddress, I2CTransactionType type);
  int endTransaction();
  int write(uint8_t* writeBuffer, int bufferLength);
  int read(uint8_t* readBuffer, int bufferLength);

private:
  int i2c_file;
  const char* filename;
};

#endif
