/*
 * FLIR Lepton3 SDK
 */

#include "YoctoI2CCommunicator.h"

YoctoI2CCommunicator::YoctoI2CCommunicator(const char* filename)
{
  this->filename = filename;
  // Open bus from userspace
  if ((this->i2c_file = open(filename, O_RDWR)) < 0)
  {
    perror("Failed to open the i2c bus\n");
  }
}

YoctoI2CCommunicator::~YoctoI2CCommunicator()
{
  // Close bus
  close(this->i2c_file);
}

int YoctoI2CCommunicator::beginTransaction(uint8_t slaveAddress, I2CTransactionType type)
{
  if (this->getTransactionType() != I2CTransactionType::undefined)
  {
    return -1;
  }
  this->setSlaveAddress(slaveAddress);
  if (this->getSlaveAddress() == I2C_INVALID_SLAVE)
  {
    return -1;
  }
  this->setTransactionType(type);
  switch (this->getTransactionType())
  {
    case I2CTransactionType::I2C_Read:
    case I2CTransactionType::I2C_Write:
      // Acquire bus access
  	  if (ioctl(this->i2c_file,I2C_SLAVE,slaveAddress) < 0)
  	  {
    		printf("Failed to acquire bus access and/or talk to slave.\n");
    		return -1;
  	  }
      return 0;
    default:
      return -1;
  }
}

int YoctoI2CCommunicator::endTransaction()
{
  this->setTransactionType(I2CTransactionType::undefined);
  return 0;
}

int YoctoI2CCommunicator::write(uint8_t* writeBuffer, int bufferLength)
{
  if (this->getTransactionType() != I2CTransactionType::I2C_Write)
  {
    return -1;
  }
  return ::write(this->i2c_file, writeBuffer, bufferLength);
}

int YoctoI2CCommunicator::read(uint8_t* readBuffer, int bufferLength)
{
  if (this->getTransactionType() == I2CTransactionType::undefined)
  {
    return -1;
  }
  else if (this->getTransactionType() == I2CTransactionType::I2C_Write)
  {
    this->setTransactionType(I2CTransactionType::I2C_Read);
  }
  int bytesRead = ::read(this->i2c_file, readBuffer, bufferLength);

  printf("%i Bytes read: ", bytesRead);
  for(int i = 0; i < bytesRead; ++i)
  {
    printf("%x ", readBuffer[i]);
  }
  printf("\n");

  return bytesRead;
}
