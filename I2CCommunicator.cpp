/*
 * FLIR Lepton3 SDK
 */

#include "I2CCommunicator.h"

void I2CCommunicator::setTransactionType(I2CTransactionType type)
{
  this->transactionType = type;
}

I2CTransactionType I2CCommunicator::getTransactionType()
{
  return this->transactionType;
}

void I2CCommunicator::setSlaveAddress(uint8_t address)
{
  this->slaveAddress = address;
}

uint8_t I2CCommunicator::getSlaveAddress()
{
  return this->slaveAddress;
}
