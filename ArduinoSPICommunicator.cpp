/*
 * FLIR Lepton3 SDK
 */

#include "ArduinoSPICommunicator.h"
#include <SPI.h>
#include <Arduino.h>

ArduinoSPICommunicator::ArduinoSPICommunicator(byte csPin, SPISettings settings)
{
  this->csPin = csPin;
  this->settings = settings;
  this->isTransactionActive = false;
  digitalWrite(csPin, HIGH);
  pinMode(csPin, OUTPUT);
  SPI.begin();
}

int ArduinoSPICommunicator::beginTransaction()
{
  if (this->isTransactionActive) return -1;
  digitalWrite(this->csPin, LOW);
  SPI.beginTransaction(this->settings);
  this->isTransactionActive = true;
  return 0;
}

int ArduinoSPICommunicator::endTransaction()
{
  if (!this->isTransactionActive) return -1;
  digitalWrite(this->csPin, HIGH);
  SPI.endTransaction();
  this->isTransactionActive = false;
  return 0;
}

int ArduinoSPICommunicator::write(byte* writeBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;

  for(int i=0; i<bufferLength; ++i)
  {
    SPI.transfer(writeBuffer[i]);
  }
  return bufferLength;
}

int ArduinoSPICommunicator::read(byte* readBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;
  for(int i=0; i<bufferLength; ++i)
  {
    readBuffer[i] = SPI.transfer(0x00);
  }
  return bufferLength;
}

int ArduinoSPICommunicator::transfer(byte* writeBuffer, byte* readBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;

  for(int i=0; i<bufferLength; ++i)
  {
    readBuffer[i] = SPI.transfer(writeBuffer[i]);
  }
  return bufferLength;
}

