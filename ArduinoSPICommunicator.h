/*
 * FLIR Lepton3 SDK
 */

#ifndef ARDUINO_SPI_COMMUNICATOR_H
#define ARDUINO_SPI_COMMUNICATOR_H

#ifndef ARDUINO
#error "Arduino classes are not available on other platforms."
#endif

#include <Arduino.h>
#include <SPI.h>
#include "SPICommunicator.h"

class ArduinoSPICommunicator : public SPICommunicator
{
public:
  ArduinoSPICommunicator(uint8_t csPin, SPISettings settings);
  int beginTransaction();
  int endTransaction();
  int write(uint8_t* writeBuffer, int bufferLength);
  int read(uint8_t* readBuffer, int bufferLength);
  int transfer(uint8_t* writeBuffer, uint8_t* readBuffer, int bufferLength);
private:
  uint8_t csPin;
  SPISettings settings;
  bool isTransactionActive;
};

#endif

