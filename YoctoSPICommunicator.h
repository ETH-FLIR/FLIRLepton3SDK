/*
 * FLIR Lepton3 SDK
 */

#ifndef YOCTO_SPI_COMMUNICATOR_H
#define YOCTO_SPI_COMMUNICATOR_H

#include <stdio.h>

#include "SPICommunicator.h"

class YoctoSPICommunicator : public SPICommunicator
{
public:
  YoctoSPICommunicator(uint8_t csPin);
  int beginTransaction();
  int endTransaction();
  int write(uint8_t* writeBuffer, int bufferLength);
  int read(uint8_t* readBuffer, int bufferLength);
  int transfer(uint8_t* writeBuffer, uint8_t* readBuffer, int bufferLength);
private:
  uint8_t csPin;
  bool isTransactionActive;
};

#endif

