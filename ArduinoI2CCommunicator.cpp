/*
 * FLIR Lepton3 SDK
 */

#include "ArduinoI2CCommunicator.h"
#include <Wire.h>
#include <Arduino.h>


ArduinoI2CCommunicator::ArduinoI2CCommunicator()
{
  Wire.begin();
}

int ArduinoI2CCommunicator::beginTransaction(uint8_t slaveAddress, I2CTransactionType type)
{
  if (this->getTransactionType() != I2CTransactionType::undefined)
  {
    return -1;
  }
  this->setSlaveAddress(slaveAddress);
  if (this->getSlaveAddress() == I2C_INVALID_SLAVE)
  {
    return -1;
  }
  this->setTransactionType(type);
  switch (this->getTransactionType())
  {
    case I2CTransactionType::I2C_Read:
      return 0;
    case I2CTransactionType::I2C_Write:
      Wire.beginTransmission(slaveAddress);
      return 0;
    default:
      return -1;
  }
}

int ArduinoI2CCommunicator::endTransaction()
{
  if(this->getTransactionType() == I2CTransactionType::I2C_Write)
  {
    Wire.endTransmission();
  }
  this->setTransactionType(I2CTransactionType::undefined);
  return 0;
}

int ArduinoI2CCommunicator::write(uint8_t* writeBuffer, int bufferLength)
{
  if (this->getTransactionType() != I2CTransactionType::I2C_Write)
  {
    return -1;
  }
  Wire.write(writeBuffer, bufferLength);
  return bufferLength;
}

int ArduinoI2CCommunicator::read(uint8_t* readBuffer, int bufferLength)
{
  if (this->getTransactionType() == I2CTransactionType::undefined)
  {
    return -1;
  }
  else if (this->getTransactionType() == I2CTransactionType::I2C_Write)
  {
    Wire.endTransmission(false);
    this->setTransactionType(I2CTransactionType::I2C_Read);
  }

  Wire.requestFrom((int)this->slaveAddress, bufferLength);
  int counter = 0;
  while(Wire.available() && counter < bufferLength)
  {
    readBuffer[counter++] = Wire.read();
  }
  return counter;
}
