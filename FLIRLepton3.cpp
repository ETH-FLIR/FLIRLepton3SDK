/*
 * FLIR Lepton3 SDK
 */

#include "FLIRLepton3.h"

#ifdef ARDUINO
#include <Arduino.h>
#else
#include <sys/time.h>
#include <thread>
#include <chrono>
#endif

FLIRLepton3::FLIRLepton3(I2CCommunicator& i2cCommunicator, SPICommunicator& spiCommunicator)
 : i2cCommunicator(i2cCommunicator), spiCommunicator(spiCommunicator)
{
}

void FLIRLepton3::resetVoSPI()
{
#ifdef ARDUINO
  spiCommunicator.endTransaction();
  SPI.end();
  delay(186);
  SPI.begin();
  spiCommunicator.beginTransaction();
#else
  // TODO: implement video reset on yocto.
#endif
}

VideoMode FLIRLepton3::getVideoMode()
{
  uint8_t data[2];
  this->readCommand(generateCommandCode(LEP3_OEM_VIDEO_OUTPUT_FORMAT, LEP3_I2C_COMMAND_TYPE_GET), data, 2);
  if (data[1] == VideoMode::RAW14)
  {
    return VideoMode::RAW14;
  }
  else if (data[1] == VideoMode::RGB888)
  {
    return VideoMode::RGB888;
  }
  return VideoMode::Unsupported;
}

void FLIRLepton3::setVideoMode(VideoMode mode)
{
  this->setAGCEnabled(mode == VideoMode::RGB888);
  uint8_t data[2];
  data[0] = 0;
  data[1] = mode;
  this->writeCommand(generateCommandCode(LEP3_OEM_VIDEO_OUTPUT_FORMAT, LEP3_I2C_COMMAND_TYPE_SET), data, 2);
  resetVoSPI();
}

uint64_t FLIRLepton3::getSerialNumber()
{
  uint64_t serialNumber;
  uint8_t serialNumberArray[8];
  this->readCommand(generateCommandCode(LEP3_SYS_FLIR_SERIAL_NUMBER, LEP3_I2C_COMMAND_TYPE_GET), serialNumberArray, 8);

  serialNumber = 0;
  printf("S/N: ");
  for(uint8_t i = 0; i < 8; ++i)
  {
    printf("%x ", serialNumberArray[i]);
    serialNumber |= ((uint64_t)(serialNumberArray[i]) << 8*i);
  }
  printf("\n");

  return serialNumber;
}

void FLIRLepton3::printFrame()
{
  
  spiCommunicator.beginTransaction();
#ifdef ARDUINO 
  Serial.println("getting SPI Packet");
#else
  printf("getting SPI Packet");
#endif
  byte leptonFrames[60][64][3];
  for(int j = 0; j < 60; ++j)
  {
    do
    {
      spiCommunicator.read(&leptonFrames[j][0][0], 244);
    } while(((leptonFrames[j][0][0] & 0x0F) == 0x0F) || (leptonFrames[j][0][1] != (j + 1)));
  }
  spiCommunicator.endTransaction();
  for(int j = 0; j < 60; ++j)
  {
    for(int i = 0; i < 64; ++i)
    {
      for(int k = 0; k < 3; ++k)
      {
#ifdef ARDUINO
        Serial.print(leptonFrames[j][i][k]);
        Serial.print(", ");
#else
        printf("%i, ", leptonFrames[j][i][k]);
#endif
      }
    }
#ifdef ARDUINO
    Serial.println();
#else
    printf("\n");
#endif
  }
}

// ------ protected functions ------

bool FLIRLepton3::getAGCEnabled()
{
  uint8_t data[2];
  this->readCommand(generateCommandCode(LEP3_AGC_ENABLE_STATE, LEP3_I2C_COMMAND_TYPE_GET), data, 2);
  return data[1] > 0;
}

void FLIRLepton3::setAGCEnabled(bool state)
{
  uint8_t data[2];
  data[0] = 0;
  data[1] = state ? 0x01 : 0x00;
  this->writeCommand(generateCommandCode(LEP3_AGC_ENABLE_STATE, LEP3_I2C_COMMAND_TYPE_SET), data, 2);
}

// ------ private functions ------

// generate command code according to datasheet
uint16_t FLIRLepton3::generateCommandCode(uint16_t cmdID, uint16_t cmdType)
{
   return (cmdID & LEP3_I2C_COMMAND_MODULE_ID_BIT_MASK) | (cmdID & LEP3_I2C_COMMAND_ID_BIT_MASK) | (cmdType & LEP3_I2C_COMMAND_TYPE_BIT_MASK);
}

void FLIRLepton3::writeCommand(uint16_t command, uint8_t *buffer, int bufferLength)
{
  if (bufferLength > 0)
  {
    this->writeRegister(LEP3_I2C_DATA_LENGTH_REG, bufferLength);
    uint16_t dataRegisterAddress = LEP3_I2C_DATA_0_REG;

    uint8_t dataRegisterAddressArray[2];
    dataRegisterAddressArray[0] = dataRegisterAddress >> 8;
    dataRegisterAddressArray[1] = dataRegisterAddress & 0xff;

    while (bufferLength > 0)
    {
      this->i2cCommunicator.beginTransaction(LEP3_I2C_DEVICE_ADDRESS, I2CTransactionType::I2C_Write);
      this->i2cCommunicator.write(dataRegisterAddressArray, 2);
      this->i2cCommunicator.write(buffer++, 2);
      this->i2cCommunicator.endTransaction();
      dataRegisterAddress += 2;
      bufferLength -= 2;
    }
    this->writeRegister(LEP3_I2C_COMMAND_REG, command);
  }
}

// Send read command to command register and read data from data register
void FLIRLepton3::readCommand(uint16_t command, uint8_t *buffer, int bufferLength)
{
  //printf("readCommand()\n");
  if (this->waitCommandReady(LEP3_I2C_MAXRETRY))
  {
    if (this->writeRegister(LEP3_I2C_COMMAND_REG, command))
    {
      if (this->waitCommandFinished(LEP3_I2C_MAXRETRY))
      {
        this->readData(buffer, bufferLength);
      }
    }
  }
}

// Check status register of FLIR camera to see if camera is busy.
// If busy bit is not set, the camera can
bool FLIRLepton3::waitCommandReady(int maxRetry)
{
  uint16_t status;
  if (!this->readRegister(LEP3_I2C_STATUS_REG, &status))
  {
    // There was an error while reading the status register
    return false;
  }

  if (!(status & LEP3_I2C_STATUS_BUSY_BIT_MASK))
  {
    return true;
  }

  int counter = 0;
  while (counter < maxRetry)
  {
#ifdef ARDUINO
    delay(1);
#else
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
#endif
    if (!this->readRegister(LEP3_I2C_STATUS_REG, &status))
    {
      // There was an error while reading the status register
      return false;
    }
    if (!(status & LEP3_I2C_STATUS_BUSY_BIT_MASK))
    {
      // Lepton3 camera is ready to proceed a new command
      return true;
    }
    ++counter;
  }
  // Lepton3 camera took too long to get ready. Abort.
  return false;
}

// Check status register of FLIR camera to see if camera is busy.
// If busy bit is not set, the camera has finished with the last command.
bool FLIRLepton3::waitCommandFinished(int maxRetry)
{
  uint16_t status;
  if (!this->readRegister(LEP3_I2C_STATUS_REG, &status))
  {
    // There was an error while reading the status register
    return false;
  }

  if (!(status & LEP3_I2C_STATUS_BUSY_BIT_MASK))
  {
    return true;
  }

  int counter = 0;
  while (counter < maxRetry)
  {
#ifdef ARDUINO
    delay(1);
#else
    std::this_thread::sleep_for(std::chrono::milliseconds(1));
#endif
    if (!this->readRegister(LEP3_I2C_STATUS_REG, &status))
    {

      return false;
    }
    if (!(status & LEP3_I2C_STATUS_BUSY_BIT_MASK))
    {
      // Lepton3 camera is ready to proceed a new command
      return true;
    }
    ++counter;
  }
  // Lepton3 camera took too long proceeding with the given command. Abort.
  return false;
}

bool FLIRLepton3::writeRegister(uint16_t reg, uint16_t value)
{
  // create array to ensure big-endian byte order.
  uint8_t regArray[2];
  uint8_t valueArray[2];

  regArray[0] = reg >> 8;
  regArray[1] = reg & 0xff;
  valueArray[0] = value >> 8;
  valueArray[1] = value & 0xff;

  this->i2cCommunicator.beginTransaction(LEP3_I2C_DEVICE_ADDRESS, I2CTransactionType::I2C_Write);
  int bytesWritten = this->i2cCommunicator.write(regArray, 2);
  bytesWritten += this->i2cCommunicator.write(valueArray, 2);

  return this->i2cCommunicator.endTransaction() == 0;
}

bool FLIRLepton3::readRegister(uint16_t reg, uint16_t *value)
{
  // create array to ensure big-endian byte order.
  uint8_t regArray[2];
  uint8_t valueArray[2];

  regArray[0] = reg >> 8;
  regArray[1] = reg & 0xff;

  this->i2cCommunicator.beginTransaction(LEP3_I2C_DEVICE_ADDRESS, I2CTransactionType::I2C_Write);
  int bytesWritten = this->i2cCommunicator.write(regArray, 2);
  int bytesRead = this->i2cCommunicator.read(valueArray, 2);
  *value = (valueArray[0] << 8) | valueArray[1];

  return this->i2cCommunicator.endTransaction() == 0;
}

int FLIRLepton3::readData(uint8_t *buffer, int bufferLength)
{
  if (bufferLength > 16) return -1;

  this->writeRegister(LEP3_I2C_DATA_LENGTH_REG, bufferLength);
  
  this->i2cCommunicator.beginTransaction(LEP3_I2C_DEVICE_ADDRESS, I2CTransactionType::I2C_Read);
  int bytesRead = this->i2cCommunicator.read(buffer, bufferLength);

  return this->i2cCommunicator.endTransaction() == 0;
}
