/*
 * FLIR Lepton3 SDK
 */

#ifndef ARDUINO_I2C_COMMUNICATOR_H
#define ARDUINO_I2C_COMMUNICATOR_H

#ifndef ARDUINO
#error "Arduino classes are not available on other platforms."
#endif

#include <Arduino.h>
#include "I2CCommunicator.h"

class ArduinoI2CCommunicator : public I2CCommunicator
{
public:
  ArduinoI2CCommunicator();
  int beginTransaction(uint8_t slaveAddress, I2CTransactionType type);
  int endTransaction();
  int write(uint8_t* writeBuffer, int bufferLength);
  int read(uint8_t* readBuffer, int bufferLength);
};

#endif
