/*
 * FLIR Lepton3 SDK
 */

#ifndef FLIR_LEPTON3_SPI_COMMUNICATOR_H
#define FLIR_LEPTON3_SPI_COMMUNICATOR_H

#include <stdint.h>

class SPICommunicator
{
public:
  // Use constructor for interface intitialization
  virtual int beginTransaction() = 0;
  virtual int endTransaction() = 0;

  virtual int write(uint8_t* writeBuffer, int bufferLength) = 0;
  virtual int read(uint8_t* readBuffer, int bufferLength) = 0;
  virtual int transfer(uint8_t* writeBuffer, uint8_t* readBuffer, int bufferLength) = 0;
};

#endif
