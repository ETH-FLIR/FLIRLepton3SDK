/*
 * FLIR Lepton3 SDK
 */

#include "YoctoSPICommunicator.h"

YoctoSPICommunicator::YoctoSPICommunicator(uint8_t csPin)
{
  // not implemented yet.
}

int YoctoSPICommunicator::beginTransaction()
{
  if (this->isTransactionActive) return -1;

  // not implemented yet.
  return 0;
}

int YoctoSPICommunicator::endTransaction()
{
  if (!this->isTransactionActive) return -1;

  // not implemented yet.
  return 0;
}

int YoctoSPICommunicator::write(uint8_t* writeBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;

  // not implemented yet.
  return 0;
}

int YoctoSPICommunicator::read(uint8_t* readBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;

  // not implemented yet.
  return 0;
}

int YoctoSPICommunicator::transfer(uint8_t* writeBuffer, uint8_t* readBuffer, int bufferLength)
{
  if (!this->isTransactionActive) return -1;

  // not implemented yet.
  return 0;
}
