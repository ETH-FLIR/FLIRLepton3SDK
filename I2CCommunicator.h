/*
 * FLIR Lepton3 SDK
 */

#ifndef FLIR_LEPTON3_I2C_COMMUNICATOR_H
#define FLIR_LEPTON3_I2C_COMMUNICATOR_H

#include <stdint.h>

#define I2C_INVALID_SLAVE (uint8_t)0xff

enum I2CTransactionType { undefined = -1, I2C_Write = 0, I2C_Read = 1 };

class I2CCommunicator
{
public:
  virtual int beginTransaction(uint8_t slaveAddress, I2CTransactionType type) = 0;
  virtual int endTransaction() = 0;
  virtual int write(uint8_t* writeBuffer, int bufferLength) = 0;
  virtual int read(uint8_t* readBuffer, int bufferLength) = 0;

protected:
  void setTransactionType(I2CTransactionType type);
  I2CTransactionType getTransactionType();
  void setSlaveAddress(uint8_t address);
  uint8_t getSlaveAddress();

  I2CTransactionType transactionType = I2CTransactionType::undefined;
  uint8_t slaveAddress = I2C_INVALID_SLAVE;
};

#endif
