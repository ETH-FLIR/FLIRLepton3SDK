/*
 * FLIR Lepton3 SDK
 */

#ifndef FLIR_LEPTON3_H
#define FLIR_LEPTON3_H

#include <stdio.h>
#include <stdint.h>

#include "FLIRLepton3Defs.h"

#include "I2CCommunicator.h"
#include "SPICommunicator.h"

#ifdef ARDUINO
#include "ArduinoI2CCommunicator.h"
#include "ArduinoSPICommunicator.h"
#else
#include "YoctoI2CCommunicator.h"
#include "YoctoSPICommunicator.h"
#endif

class FLIRLepton3
{
public:
  FLIRLepton3(I2CCommunicator& i2cCommunicator, SPICommunicator& spiCommunicator);
    
  uint64_t getSerialNumber();
  
  void resetVoSPI();
  VideoMode getVideoMode();
  void setVideoMode(VideoMode mode);
  void printFrame();
  
protected:
  bool getAGCEnabled();
  void setAGCEnabled(bool state);
  
private:
  I2CCommunicator& i2cCommunicator;
  SPICommunicator& spiCommunicator;

  uint16_t generateCommandCode(uint16_t cmdID, uint16_t cmdType);
  void writeCommand(uint16_t command, uint8_t *buffer, int bufferLength);
  void readCommand(uint16_t command, uint8_t *buffer, int bufferLength);

  bool waitCommandReady(int timeout);
  bool waitCommandFinished(int timeout);
  bool writeRegister(uint16_t reg, uint16_t value);
  bool readRegister(uint16_t reg, uint16_t *value);

  int readData(uint8_t *buffer, int bufferLength);
};

#endif
